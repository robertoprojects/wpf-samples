﻿using mvvm_getstarted.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mvvm_getstarted
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void CustomerViewControl_Loaded(object sender, RoutedEventArgs e)
        {
            CustomerViewModel customerViewModelObject = new CustomerViewModel();
            customerViewModelObject.GetCustomers();
            CustomerViewControl.DataContext = customerViewModelObject;
        }
    }
}
