﻿using System;
using System.Threading;

namespace name_a_mutex
{
    class Program
    {
        static void Main(string[] args)
        {
            string mutexName = "test";
            Mutex m = new Mutex(false, mutexName);
            for(;;)
            {
                m.WaitOne();
                Console.WriteLine("Have Mutex");
                Console.WriteLine("Releasing");
                m.ReleaseMutex();
            }
            Console.Read();
        }
    }
}
