﻿using System;
using System.Threading;

namespace own_a_mutex
{
    class Program
    {
        static void Main(string[] args)
        {
            bool ownsMutex;
            using (Mutex mutex = new Mutex(true, "MutexExample", out ownsMutex))
            {
                if (ownsMutex)
                {
                    Console.WriteLine("Owned");
                    mutex.ReleaseMutex();
                }
                else
                {
                    Console.WriteLine("Another instance of this application " + " already owns the mutex named MutexExample.");
                }
            }
            Console.Read();
        }
    }
}
