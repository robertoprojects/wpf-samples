﻿using System.ComponentModel;

namespace mvvm_getstarted.Models
{
    public class CustomerModel{}

    public class Customer : INotifyPropertyChanged
    {
        private string customerFirstName;
        private string customerLastName;

        public string CustomerFirstName
        {
            get { return customerFirstName; }
            set
            {
                if (customerFirstName != value)
                {
                    customerFirstName = value;
                    RaisePropertyChanged("CustomerFirstName");
                    RaisePropertyChanged("FullName");
                }
            }
        }

        public string CustomerLastName
        {
            get { return customerLastName; }
            set
            {
                if (customerLastName != value)
                {
                    customerLastName = value;
                    RaisePropertyChanged("CustomerLastName");
                    RaisePropertyChanged("FullName");
                }
            }
        }

        public string FullName
        {
            get { return customerFirstName + " " + customerLastName; }        
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
