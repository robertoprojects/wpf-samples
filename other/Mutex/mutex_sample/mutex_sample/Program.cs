﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace mutex_sample
{
    class Program
    {
        private static Mutex mutex = new Mutex();
        private const int numHits = 1;
        private const int numThreads = 4;

        private static void ThreadProcess()
        {
            for (int i = 0; i < numHits; i++)
            {
                UseCsharpcorner();
            }
        }

        private static void UseCsharpcorner()
        {
            mutex.WaitOne();
            Console.WriteLine("{0} has entered in the C_sharpcorner.com", Thread.CurrentThread.Name);
            Thread.Sleep(2000);
            Console.WriteLine("{0} is leaving the C_sharpcorner.com\r\n", Thread.CurrentThread.Name);
            mutex.ReleaseMutex();
        }

        static void Main(string[] args)
        {
            for (int i = 0; i < numThreads; i++)
            {
                Thread myCorner = new Thread(new ThreadStart(ThreadProcess));
                myCorner.Name = String.Format("Thread{0}", i + 1);
                myCorner.Start();
            }
            Console.Read();
        }
    }
}
