﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Bson.Serialization;

namespace MongoDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ReturnNumbers();
        }
        async private void ReturnNumbers()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("tutorial");
            var collection = database.GetCollection<BsonDocument>("numbers");
            var numbers = database.GetCollection<Numbers>("numbers");

            var list = await numbers.Find(_ => true).ToListAsync();
            var list2 = await collection.Find(_ => true).ToListAsync();

            List<Numbers> listNumbers = BsonSerializer.Deserialize<List<Numbers>>(list.ToJson());
            List<Numbers> listNumbers2 = BsonSerializer.Deserialize<List<Numbers>>(list2.ToJson());

            Numbers num;

            //foreach (var item in list2)
            //{
            //    num = new Numbers();                
            //    lblResult.Items.Add(item["num"].AsDouble.ToString());
            //}
            foreach (Numbers n in listNumbers2)
            {
                lblResult.Items.Add(n.num);
            }
        }
    }

    public class Numbers
    {
        public ObjectId Id { get; set; }
        public int num { get; set; }
    }
}
