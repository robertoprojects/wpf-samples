﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace fork_join
{
    class Program
    {
        static void Main(string[] args)
        {
        }

        //Approach #1: One Task per element
        static T[]MyParallelInvoke1<T>(params Func<T>[] functions)
        {
            var tasks = (from function in functions select Task.Factory.StartNew(function)).ToArray();
            Task.WaitAll();
            return tasks.Select(t => t.Result).ToArray();
        }

        //Approach #2: One Task per element, using parent/child Relationships
        static T[] MyParallelInvoke2<T>(params Func<T>[] functions)
        {
            var results = new T[functions.Length];
            Task.Factory.StartNew(() =>
            {
                for (int i = 0; i < functions.Length; i++)
                {
                    int cur = i;
                    Task.Factory.StartNew(() => results[cur] = functions[cur](), TaskCreationOptions.AttachedToParent);
                }
            }).Wait();

            return results;

        }

        //Approach #3: Using Parallel.For
        static T[] MyParallelInvoke3<T>(params Func<T>[] functions)
        {
            T[] results = new T[functions.Length];
            Parallel.For(0, functions.Length, i =>
            {
                results[i] = functions[i]();
            });
            return results;
        }

        //Approach #4: Using PLINQ
        static T[] MyParallelInvoke<T>(params Func<T>[] functions)
        {
            return functions.AsParallel().Select(f => f()).ToArray();
        }
    }
}
