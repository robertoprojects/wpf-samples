﻿using mvvm_getstarted.Models;
using System.Collections.ObjectModel;

namespace mvvm_getstarted.ViewModel
{
    public class CustomerViewModel
    {
        public ObservableCollection <Customer> Customers
        {
            get;
            set;
        }

        public void GetCustomers()
        {
            ObservableCollection<Customer> customers = new ObservableCollection<Customer>();
            customers.Add(new Customer { CustomerFirstName = "Neha", CustomerLastName="Jangra"});
            customers.Add(new Customer { CustomerFirstName = "Test", CustomerLastName = "Test" });
            customers.Add(new Customer { CustomerFirstName = "Test1", CustomerLastName = "Test2" });
            Customers = customers;
        }
    }
}
