﻿using System;
using System.Threading;

namespace mutex_shared_resource
{
    class MyCounter
    {
        public static int count = 0;
        public static Mutex MutexLock = new Mutex();
    }
    class IncThread
    {
        public Thread th;
        public IncThread()
        {
            th = new Thread(this.GO);
            th.Start();
        }
        void GO()
        {
            Console.WriteLine("IncThread is waiting for the mutex.");
            MyCounter.MutexLock.WaitOne();
            Console.WriteLine("IncThread acquires the mutex.");
            int num = 10;
            do
            {
                Thread.Sleep(500);
                MyCounter.count++;
                Console.WriteLine("In IncThread, MyCounter.count is " + MyCounter.count);
                num--;
            } while (num > 0);
            Console.WriteLine("IncThread releases the mutex.");
            MyCounter.MutexLock.ReleaseMutex();
        }
    }

    class DecThread
    {
        public Thread th;
        public DecThread()
        {
            th = new Thread(new ThreadStart(this.Go));
            th.Start();
        }
        void Go()
        {
            Console.WriteLine("DecThread is waiting for the mutex.");
            MyCounter.MutexLock.WaitOne();
            Console.WriteLine("DecThread acquires the mutex.");
            int num = 10;
            do
            {
                Thread.Sleep(500);
                MyCounter.count--;
                Console.WriteLine("In DecThread, MyCounter.count is " + MyCounter.count);
                num--;
            } while (num > 0);
            Console.WriteLine("DecThread releases the mutex.");
            MyCounter.MutexLock.ReleaseMutex();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            IncThread myt1 = new IncThread();
            DecThread myt2 = new DecThread();
            myt1.th.Join();
            myt2.th.Join();
            Console.Read();
        }
    }
}
